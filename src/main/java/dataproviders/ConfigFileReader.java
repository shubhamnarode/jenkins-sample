package dataproviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import enums.DriverType;


/**
 * This class reads data from properties file. 
 * @author shubham.narode
 *
 */
public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= "configurations//configuration.properties";
	
	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}	
	
	/**
	 * This method provides driver path
	 * @return String
	 */
	public String getFirefoxDriverPath(){
		String driverPath = properties.getProperty("linuxFirefoxDriverPath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Firefox driverPath not specified in the Configuration.properties file.");		
	}
	public String getChromeDriverPath(){
		String driverPath = properties.getProperty("chromeDriverPath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("ChromedriverPath not specified in the Configuration.properties file.");		
	}
	public String getIeDriverPath(){
		String driverPath = properties.getProperty("iedriverPath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("IEdriverPath not specified in the Configuration.properties file.");		
	}
	
	/**
	 * This method provides Driver type
	 * @return DriverType
	 */	
	public DriverType getBrowser() {
		String browserName = properties.getProperty("browser");
		if(browserName.equals("chrome")) return DriverType.CHROME;
		else if(browserName == null || browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX;
		else if(browserName.equals("ie")) return DriverType.INTERNETEXPLORER;
		else throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName);
	}
	
	/**
	 * This method returns Base Url of Application 
	 * @return String
	 */
	public String getApplicationUrl() {
		String baseUrl = properties.getProperty("baseUrl");
		if(baseUrl != null) return baseUrl;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
	
	/**
	 * This method gets browserWindowSize
	 * @return true if browser window is maximized
	 */
	
	public Boolean getBrowserWindowSize() {
		String windowSize = properties.getProperty("windowMaximize");
		if(windowSize != null) return Boolean.valueOf(windowSize);
		return true;
	}
	

	/**
	 * This method returns location where downloaded file will be stored. 
	 * @return String
	 */
	public String getFileDownloadPath() {
		String path = properties.getProperty("fileDownloadPath");
		if(path != null) return path;
		else throw new RuntimeException("url not specified in the Configuration.properties file.");
	}
}