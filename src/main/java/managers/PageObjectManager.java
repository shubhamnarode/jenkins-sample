package managers;

import org.openqa.selenium.WebDriver;

import pages.WishListPage;
import pages.LoginPage;
import pages.ShoppingCartPage;

public class PageObjectManager {

	private WebDriver driver;
	private LoginPage loginPage;
	private WishListPage wishListPage;
	private ShoppingCartPage shoppingCartPage;
	
	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}

	public LoginPage getLoginPage() {
		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
	}

	public WishListPage getWishListPage() {
		return (wishListPage == null) ? wishListPage = new WishListPage(driver) : wishListPage;
	}
	
	public ShoppingCartPage getShoppingCartPage() {
		return (shoppingCartPage == null) ? shoppingCartPage = new ShoppingCartPage(driver) : shoppingCartPage;
	}
}