package managers;

import dataproviders.ConfigFileReader;

/**
 * This is singleton class which gives access to file reader methods.
 * 
 * @author shubham.narode
 *
 */
public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static ConfigFileReader configFileReader;

	private FileReaderManager() {
	}

	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	public ConfigFileReader getConfigReader() {
		return (configFileReader == null) ? new ConfigFileReader() : configFileReader;
	}	
}