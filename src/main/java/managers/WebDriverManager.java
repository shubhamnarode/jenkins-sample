package managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import enums.DriverType;


/**
 * This is singleton class which provides perticular type of driver instance.
 * @author shubham.narode
 *
 */
public class WebDriverManager {
	private static WebDriver driver;
	private static DriverType driverType = FileReaderManager.getInstance().getConfigReader().getBrowser();

	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";

	private WebDriverManager() {
	}

	public static WebDriver getDriver() {
		if(driver == null) driver = createLocalDriver();
		return driver;
	}

	private static WebDriver createLocalDriver() {
        switch (driverType) {	    
        case FIREFOX :
        	System.setProperty(FIREFOX_DRIVER_PROPERTY, System.getProperty("user.dir") + FileReaderManager.getInstance().getConfigReader().getFirefoxDriverPath());
    		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
    		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");    		
        	driver = new FirefoxDriver();
	    	break;
        case CHROME : 
        	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + FileReaderManager.getInstance().getConfigReader().getChromeDriverPath());
        	driver = new ChromeDriver();
    		break;
        case INTERNETEXPLORER : 
        	System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + FileReaderManager.getInstance().getConfigReader().getIeDriverPath());
        	driver = new InternetExplorerDriver();
    		break;
        }   
        if(FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) {
        	driver.manage().window().maximize();
        }

		return driver;
	}	

	public void closeDriver() {
		driver.close();
		driver.quit();
	}

}