package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import managers.FileReaderManager;

/**
 * This is Page Class for Login page, contains WebElements and Actions for Login
 * Page
 * 
 * @author shubham.narode
 * 
 */
public class LoginPage extends BasePage {
	// constructor
	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Web Elements
	@FindBy(xpath = "//input[@class = '_2zrpKA']")
	private WebElement usernameField;

	@FindBy(xpath = "//input[@type = 'password']")
	private WebElement passwordField;

	@FindBy(xpath = "//button[@class = '_2AkmmA _1LctnI _7UHT_c']")
	private WebElement loginButton;

	@FindBy(xpath = "//div[text() = 'Shubham']")
	private WebElement userName;

	// Actions on login page

	/**
	 * This method Navigates control to login Page
	 */
	public void navigateToHome() {
		navigateTo(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
	}

	/**
	 * This method Enters username in Username Field.
	 * 
	 * @param username
	 */
	public void enterUserName(String username) {
		writeText(usernameField, username);
	}

	/**
	 * This method Enters password in password field.
	 * 
	 * @param password
	 */
	public void enterPassword(String password) {
		writeText(passwordField, password);
	}

	/**
	 * This method clicks on Login button.
	 */
	public void clickOnLogin() {
		clickOnElement(loginButton);
	}


	/**
	 * This method Verify successful Login.
	 */
	public void verifyLoginSuccessful() {
		waitForElementToBeVisible(userName);
		Assert.assertTrue(isElementVisible(userName));
	}
}
