package pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * This is Page Class for Shopping Cart page, contains WebElements and Actions
 * for Shopping Cart Page.
 * 
 * @author shubham.narode
 * 
 */

public class ShoppingCartPage extends BasePage {

	public ShoppingCartPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Page Variables
	int expectedTotalCost = 0;
	int expectedNumberOfProducts = 0;
	List<Integer> expectedPriceForEachProduct;

	// Locators

	By searchIconLocator = By.xpath("//button[@type='submit']//*[@width='20']");

	By numberOfProductInSearchResultLocator = By.cssSelector("div._1OCn9C");

	By goToCartLocator = By.xpath("//button[@class='_2AkmmA _2Npkh4 _2MWPVK']");

	By numberOfProductsInCartLocator = By.xpath("//div[@class = '_3ycxrs']");

	By productPriceInCartLocator = By.xpath("//span[@class=\"pMSy0p XU9vZa\"]");

	By selectMaxPriceLocator = By.cssSelector("div._1YoBfV > select");

	// WebElements
	@FindBy(xpath = "//span[text()='Cart']")
	private WebElement shoppingCartIcon;

	@FindBy(xpath = "//input[contains(@placeholder,'Search for products')]")
	private WebElement searchProductField;

	@FindBy(xpath = "//button[text()= 'ADD TO CART']")
	private List<WebElement> addToCartButton;

	@FindBy(xpath = "//button[text()= 'GO TO CART']")
	private List<WebElement> goToCartButton;

	@FindBy(xpath = "//div[text()= 'Coming Soon']")
	private List<WebElement> commingSoonProduct;

	@FindBy(xpath = "//div[text()= 'Sold Out']")
	private List<WebElement> soldOutProduct;

	@FindBy(xpath = "//div[@class='_1vC4OE _3qQ9m1']")
	private WebElement productPrice;

	@FindBy(xpath = "//div[@class='_2twTWD']/div[1]/span")
	private WebElement totalPriceInCart;

	@FindBy(xpath = "//div[@class = 'gdUKd9']/span[text()= 'Remove']")
	List<WebElement> removeProductsFromCart;
	
	// Actions on Shopping cart page
	/**
	 * Navigate to Shopping cart
	 */
	public void navigateToShoppingCart() {
		clickOnElement(shoppingCartIcon);
	}

	/**
	 * This method Search product on flipcart
	 * 
	 * @param productName
	 */
	public void serachProduct(String productName) {
		mouseHover(searchProductField);
		writeText(searchProductField, productName);
		clickOnElement(waitForElementPresence(searchIconLocator));
	}

	/**
	 * This Method Sets Price range as filter for searched product.
	 * 
	 * @param minPrice
	 * @param maxPrice
	 */
	public void setPriceRange(String minPrice, String maxPrice) {
		selectItemFromDropDown(waitForElementPresence(selectMaxPriceLocator), maxPrice);
	}

	/**
	 * This method adds all Product in search result to shopping cart
	 * 
	 * @throws InterruptedException
	 */
	public void addProductToCart() throws InterruptedException {
		String parentHandle = driver.getWindowHandle();
		Thread.sleep(400);

		List<WebElement> searchedProducts = waitForAllElementsToBePresent(numberOfProductInSearchResultLocator);
		expectedPriceForEachProduct = new ArrayList<Integer>();

		for (int iterator = 0; iterator < searchedProducts.size(); iterator++) {
			WebElement product = searchedProducts.get(iterator);
			clickOnElement(product);

			if (!isElementVisible(product)) {
				scrollToElement(product);
			}

			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
			}
			if (addToCartButton.size() > 0) {
				Thread.sleep(200);
				clickOnElement(addToCartButton.get(0));
				calculateExpectedTestData();
				navigateToParentWindow(parentHandle);
			} else if (isProductAlreadyInCart()) {
				calculateExpectedTestData();
				navigateToParentWindow(parentHandle);
			} else if (isProductCommingSoon() || isProductOutOfStock()) {
				navigateToParentWindow(parentHandle);
			}
		}
	}

	/**
	 * This method closes current window and Navigates control to parent Window
	 * 
	 * @param parentHandle
	 */
	public void navigateToParentWindow(String parentHandle) {
		driver.close();
		driver.switchTo().window(parentHandle);
	}

	public void calculateExpectedTestData() {
		expectedPriceForEachProduct.add(getPrice(productPrice));
		expectedTotalCost += getPrice(productPrice);
		expectedNumberOfProducts += 1;
	}

	/**
	 * This method gets price of each product from product information.
	 * 
	 * @param webElement
	 * @return Price of Product.
	 */
	public int getPrice(WebElement webElement) {
		Pattern pattern = Pattern.compile("[^\\d+]");
		String priceInString = webElement.getText();
		String newString = pattern.matcher(priceInString).replaceAll("");
		int price = convertStringToDouble(newString);
		return price;
	}

	/**
	 * Converts String to Integer
	 * 
	 * @param string
	 * @return integer
	 */
	public int convertStringToDouble(String string) {
		return Integer.parseInt(string);
	}

	/**
	 * Returns true if product is comming soon.
	 * 
	 * @return
	 */
	boolean isProductCommingSoon() {
		boolean flag = false;
		if (commingSoonProduct.size() != 0) {
			return true;
		}
		return flag;
	}

	/**
	 * Returns true if product is Out of stock.
	 * 
	 * @return
	 */
	boolean isProductOutOfStock() {
		boolean flag = false;
		if (soldOutProduct.size() != 0) {
			return true;
		}
		return flag;
	}

	/**
	 * Returns true if product is already in cart.
	 * 
	 * @return
	 */
	boolean isProductAlreadyInCart() {
		boolean flag = false;
		if (goToCartButton.size() != 0) {
			return true;
		}
		return flag;
	}

	/**
	 * Verify Count nd Price of Products in Shopping cart
	 */
	public void verifyShoppingCart() {
		navigateToShoppingCart();
		
		List<WebElement> numberOfProductsInCart = waitForAllElementsToBePresent(numberOfProductsInCartLocator);
		List<WebElement> productPriceInCart = waitForAllElementsToBePresent(productPriceInCartLocator);
		WebElement individualPriceElement;
		List<Integer> actualPriceForEachProduct = new ArrayList<Integer>();

		for (int iterator = 0; iterator < productPriceInCart.size(); iterator++) {
			individualPriceElement = productPriceInCart.get(iterator);
			actualPriceForEachProduct.add(getPrice(individualPriceElement));
		}
		System.out.println(numberOfProductsInCart.size());
		Assert.assertEquals(numberOfProductsInCart.size(), expectedNumberOfProducts,
				"The Number Of products added are not visible in Shopping cart");

		Assert.assertEquals(getPrice(totalPriceInCart), expectedTotalCost, "Total Price in cart is Wrong");

		Collections.reverse(actualPriceForEachProduct);
		Assert.assertTrue(isEqual(actualPriceForEachProduct, actualPriceForEachProduct),
				"Price for individual Product is wrong");
	}

	/**
	 * This method checks equality of two Lists.
	 * 
	 * @param actualPriceForEachProduct
	 * @param expectedPriceForEachProduct
	 * @return
	 */
	public boolean isEqual(List<Integer> actualPriceForEachProduct, List<Integer> expectedPriceForEachProduct) {

		if (actualPriceForEachProduct.size() != expectedPriceForEachProduct.size()) {
			return false;
		}
		List<Integer> tempList = new ArrayList<Integer>(expectedPriceForEachProduct);
		for (Integer element : actualPriceForEachProduct) {
			if (!tempList.remove(element)) {
				return false;
			}
		}
		return tempList.isEmpty();
	}
}
