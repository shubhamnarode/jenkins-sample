package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This class contains Basic functionalities which are common to All pages.
 * 
 * @author shubham.narode
 */
public class BasePage {
	protected WebDriver driver;
	private WebDriverWait wait;

	// Constructor
	public BasePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 20);
	}	

	// Common basic Actions
	
	/**
	 * This method navigates to url provided.
	 * @param baseUrl
	 */	
	public void navigateTo(String baseUrl) {
		driver.navigate().to(baseUrl);
	}
	
	/**
	 * This method waits for element to be clickable.
	 * @param element
	 */
	public void waitForElementToBeClickble(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	/**
	 * This method waits for element to be visible.
	 * @param element
	 */	
	public void waitForElementToBeVisible(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	/**
	 * This method waits for single element to be present
	 * @param locator
	 * @return
	 */
	public WebElement waitForElementPresence(By locator) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
	}
	
	/**
	 * This method waits for list of elements to be present
	 * @param locator
	 * @return
	 */
	public List<WebElement> waitForAllElementsToBePresent(By locator) {
	//	wait.until(ExpectedConditions.elementToBeClickable(locator));
		return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	/**
	 * This method returns Visibility status of WebElement.
	 * @param element
	 * @return
	 */
	public boolean isElementVisible(WebElement element) {
		waitForElementToBeVisible(element);
		return element.isDisplayed();
	}

	/**
	 * This method Scroll the page until the visibility of WebElement.
	 * @param element
	 */
	public void scrollToElement(WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("arguments[0].scrollIntoView();", element); 
	}
	
	/**
	 * This method Clicks on WebElement 	
	 * @param element
	 */
	public void clickOnElement(WebElement element) {
		waitForElementToBeClickble(element);
		element.click();
	}

	/**
	 * This method enters text in textfield
	 * @param element
	 * @param text
	 */
	public void writeText(WebElement element, String text) {
		waitForElementToBeClickble(element);
		element.clear();		
		element.sendKeys(text);
	}

	/**
	 * This method Moves mouse Pointer on WebElement.
	 * @param element
	 */
	public void mouseHover(WebElement element) {
		waitForElementToBeVisible(element);
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	/**
	 * This method selects Item from dropdown.
	 * @param element
	 * @param value
	 */
	public void selectItemFromDropDown(WebElement element, String value) {
		Select selectItem = new Select(element);
		selectItem.selectByValue(value);
	}
}
