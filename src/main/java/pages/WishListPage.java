package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * This is Page Class for WishList page, contains WebElements and Actions for
 * WishList Page
 * 
 * @author shubham.narode
 * 
 */
public class WishListPage extends BasePage {

	public WishListPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Page Variables
	int expectedNumberOfProductsInWishList = 0;

	// Locators
	By wishListIcon = By.xpath("//div[@class = '_3gDSOa _3iGnbq']/div[@class= 'DsQ2eg']");

	By numberOfProductInWishListLocator = By.xpath("//a[@class = '_3rAVLS']");

	By deleteAllProductsInWishListLocator = By.xpath("//img[@class = '_27LgAY']");

	// WebElements

	@FindBy(xpath = "//div[text() = 'Shubham']")
	private WebElement navigateOption;

	@FindBy(xpath = "//ul[@class='_3Ji-EC']//li[4]")
	private WebElement wishListOption;

	@FindBy(xpath = "//div[@class='zi6sUf _3Ed3Ub']")
	private WebElement menuRow;

	@FindBy(xpath = "//span[text()='Electronics']")
	private WebElement electronicsCategory;

	@FindBy(xpath = "//a[text() = 'Mouse']")
	private WebElement electronicsProduct;

	@FindBy(xpath = "//span[text()='Men']")
	private WebElement mensCategory;

	@FindBy(xpath = "//a[@title='T-Shirts']")
	private WebElement mensProduct;

	@FindBy(xpath = "//button[text()='YES, REMOVE']")
	private WebElement confirmDelete;
	// Actions on WishList page

	/**
	 * This method Navigate control to Wish List page.
	 */
	public void navigateToWishList() {
		mouseHover(navigateOption);
		clickOnElement(wishListOption);
	}

	public void clearWishList() throws InterruptedException {
		List<WebElement> deleteAllProductsInWishListIcon = waitForAllElementsToBePresent(
				deleteAllProductsInWishListLocator);
		if (deleteAllProductsInWishListIcon.size() >= 0) {
			for (WebElement deleteProductIcon : deleteAllProductsInWishListIcon) {
				clickOnElement(deleteProductIcon);
				Thread.sleep(200);
				clickOnElement(confirmDelete);
			}
		}
	}
	/**
	 * This method adds Product to Wish List.
	 * 
	 * @throws InterruptedException
	 */
	public void clickOnWishListIcon() throws InterruptedException {
		List<WebElement> wishListIconList = waitForAllElementsToBePresent(wishListIcon);
		for (int iterator = 0; iterator < 2; iterator++) {
			if (!wishListIconList.get(iterator).isDisplayed()) {
				scrollToElement(wishListIconList.get(iterator));
				Thread.sleep(500);
			}
			clickOnElement(wishListIconList.get(iterator));
			expectedNumberOfProductsInWishList += 1;
		}
	}

	/**
	 * This method selects Products form different Category
	 * 
	 * @throws InterruptedException
	 */
	public void addProductToWishList() throws InterruptedException {
		mouseHover(electronicsCategory);
		clickOnElement(electronicsProduct);
		clickOnWishListIcon();

		if (!isElementVisible(menuRow)) {
			scrollToElement(menuRow);
		}

		mouseHover(mensCategory);
		clickOnElement(mensProduct);
		clickOnWishListIcon();
	}

	/**
	 * This method verifies number od products are added to wishlist.
	 */
	public void verifyProductInWishList() {
		navigateToWishList();
		List<WebElement> numberOfProductsInWishListAfter = waitForAllElementsToBePresent(
				numberOfProductInWishListLocator);
		Assert.assertEquals(numberOfProductsInWishListAfter.size(), expectedNumberOfProductsInWishList,
				"Number of product added to wish list are not equal to expected");
	}
}
