package stepdefinition;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.ShoppingCartPage;

public class ShoppingCartSteps {

	ShoppingCartPage shoppingCartPage;
	TestContext testContext;

	public ShoppingCartSteps(TestContext context) {
		testContext = context;
		shoppingCartPage = testContext.getPageObjectManager().getShoppingCartPage();
	}

	@Given("User is at Shopping cart page")
	public void user_is_at_Shoppint_cart_page() {
		shoppingCartPage.navigateToShoppingCart();
	}

	@When("User searches {string} product")
	public void user_enters_to_search_products(String productName) {
		shoppingCartPage.serachProduct(productName);
	}
	
	@When("User sets price range between {string} to {string}")
	public void User_sets_price_range(String minPrice, String maxPrice) {
		shoppingCartPage.setPriceRange(minPrice, maxPrice);
	}

	@When("User Adds product in search result to cart")
	public void adds_product_in_search_result_to_cart() throws InterruptedException {
		shoppingCartPage.addProductToCart();
	}

	@Then("User should see count of product, total price and individual price")
	public void user_should_see_count_of_product_total_price_and_individual_price() {
		shoppingCartPage.verifyShoppingCart();
	}

}
