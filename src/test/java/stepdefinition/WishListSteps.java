package stepdefinition;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.WishListPage;

public class WishListSteps {
	TestContext testContext;
	WishListPage wishListPage;
	public WishListSteps(TestContext context) {
		testContext = context;
		wishListPage = testContext.getPageObjectManager().getWishListPage();
	}
	
	@Given("User is at wishlist page")
	public void user_is_at_wishlist_page() throws InterruptedException {
		wishListPage.navigateToWishList();
		wishListPage.clearWishList();
	}

	@When("User adds products in wishlist")
	public void user_adds_products_in_wishlist() throws InterruptedException {
		wishListPage.addProductToWishList();
	}

	@Then("User should see list of products in wishlist")
	public void user_should_see_list_of_products_in_wishlist() {
		wishListPage.verifyProductInWishList();
	}

}
