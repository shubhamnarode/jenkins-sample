package stepdefinition;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.LoginPage;

/**
 * This class contains StepDefinations for implementing the scenarios
 * for login into Gmail application
 *
 * @author shubham.narode
 *          
 */
public class LoginSteps {
	LoginPage loginPage;
	TestContext testContext;

	public LoginSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}
	
	@Given("User is at home page")
	public void user_is_at_home_page() {
		loginPage.navigateToHome();	 
	}

	@When("User Enters Email or Mobile number as {string}")
	public void user_Enters_Email_or_Mobile_number_as(String username) {
		loginPage.enterUserName(username);
	}

	@When("User enters password as {string}")
	public void user_enters_password_as(String password) {
		loginPage.enterPassword(password);
	}

	@When("User clicks on Login")
	public void user_clicks_on_Login() {
		loginPage.clickOnLogin();
	}
	
	@Then("User should be able to Sign In and see username as {string} on home page")
	public void user_should_be_able_to_Sign_In_and_see_username_as_on_home_page(String string) {
		loginPage.verifyLoginSuccessful();
	}

}
