package testrunner;

/**
 * @author shubham.narode
 *
 *	This class is entry point for this test 
 *	contains different cucumber options to automate test scenarios.
 */
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(monochrome = true, features = { "src//test//resources//features" }, tags= {"@verifyUserLogin"} ,glue = {
		"stepdefinition", "testrunner" },plugin = {"pretty", "html:target/cucumber-reports/cucumber-pretty",
				"json:target/cucumber-reports/CucumberTestReport.json"} )
public class TestRunner extends AbstractTestNGCucumberTests {

}
