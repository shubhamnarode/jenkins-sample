package testrunner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import managers.WebDriverManager;

public class Hooks {

	WebDriver driver = WebDriverManager.getDriver();

	@After
	public void takeScreenShotAfterScenario(Scenario scenario) {
		if (scenario.isFailed()) {
			TakesScreenshot screenShot = (TakesScreenshot) driver;
			File sourceFile = screenShot.getScreenshotAs(OutputType.FILE);
			File destinationFile = new File(".//screenshots//screenshot_" + timestamp() + ".png");
			try {
				FileUtils.copyFile(sourceFile, destinationFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public static String timestamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
	}

}
