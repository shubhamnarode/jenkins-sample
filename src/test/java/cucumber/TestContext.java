package cucumber;

import managers.PageObjectManager;
import managers.WebDriverManager;

/**
 * This class creates common context for cucmber steps 
 * @author shubham.narode
 *
 */
public class TestContext {
	private PageObjectManager pageObjectManager;
	
	public TestContext(){
		pageObjectManager = new PageObjectManager(WebDriverManager.getDriver());
	}
	
	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}
}