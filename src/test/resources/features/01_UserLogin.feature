Feature: Login functionality for Flipcart application 
	The user should be able to login into the flipcart account if the username and the password are correct.

@verifyUserLogin
Scenario: Verify user should be able to Sign In with valid credentials and should see username on home page 
	Given User is at home page 
	When User Enters Email or Mobile number as "8999579890" 
	And User enters password as "December@30" 
	And User clicks on Login 
	Then User should be able to Sign In and see username as "Shubham" on home page 
