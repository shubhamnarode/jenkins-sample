Feature: Flipcart Wishlist functionality 

@addToWishListTest
Scenario: Add product in wishlist and verify product is added successfully. 
	Given User is at wishlist page 
	When User adds products in wishlist 
	Then User should see list of products in wishlist 
	
