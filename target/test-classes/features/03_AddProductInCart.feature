Feature: FLipcart Add to Shopping cart functionality 

@addToShoppingCartTest
Scenario: Add products in a cart based on Search result and verify count, total price and individual price of products 
	Given User is at Shopping cart page	
	When User searches "iphone6" product
	And User sets price range between "10000" to "30000"
	And User Adds product in search result to cart 
	Then User should see count of product, total price and individual price
	
	
	